import { createLocalStorage } from '@/srv/cache.js'
import { updateUserParameter } from '@/srv/user.js'

const storage = createLocalStorage()
const onlinePersistanceConfig = {
  defaultMapZoomLevel: {
    type: 'userParameter',
  },
  userBaseLayer: {
    type: 'userParameter',
  }
}

/**
 * Uses browser client-side storage.
 * @param {String} name
 * @param {Any} value
 * @param {String} options.suffix Optional
 * @returns
 */
export async function saveSettingsParamLocally(name, value, options = {}) {
  
  if(typeof value ==='object'){
    value = JSON.parse(JSON.stringify(value)) //remove reactivity
  }

    console.log('saveSettingsParamLocally', {
      name: name,
      value: value,
      options
    })
  
  let key = name + (options.suffix ? `_${options.suffix}` : '')
  await storage.setItem(key, value)
}

/**
 * Used to save defaultMapZoomLevel and userBaseLayer
 * Used to save defaultMapPredefinedView
 * @param {*} name
 * @param {*} value
 * @returns
 */
export async function saveSettingsParamRemotely(
  code,
  value,
  isLoginAs,
  userParameters = []
) {
  /*if (isLoginAs) {
    return console.log(
      'saveSettingsParamRemotely::skip (login-as)',
      code,
      value
    )
  }*/

  if (onlinePersistanceConfig[code]) {
    //console.log('saveSettingsParamRemotely', code, value)

    if (onlinePersistanceConfig[code].type === 'userParameter') {
      const id = userParameters.find((i) => i.code === code)?.id || null

      if (!id) {
        console.warn(
          'Unable to save parameter online because there is no related id'
        )
        return
      }

      let res = false
      try {
        await updateUserParameter({
          id,
          parameter: code,
          value: value,
        })
        /*console.log(
          'saveSettingsParamRemotely::updateUserParameter',
          code,
          value
        )*/
      } catch (err) {
        console.warn('saveSettingsParamRemotely::updateUserParameter::error', {
          code: code,
          value: value,
          error: err.stack,
        })
      }
      return res
    }
    if (onlinePersistanceConfig[code].type === 'custom') {
      /*console.log('saveSettingsParamRemotely::custom', code, value)*/
      return await onlinePersistanceConfig[code].update(value)
    }
  }
}

/**
 * Used to retrieve defaultMapPredefinedView
 * Parses onlinePersistanceConfig and call 'retrieve' method for each key.
 * @param {Boolean} context.isLoginAs
 * @returns {Object} Object key/value pairs i.g {defaultMapPredefinedView:1}
 */
export async function getSettingsParamFromRemote(context = {}) {
  let onlineSettings = {}
  await Promise.all(
    Object.keys(onlinePersistanceConfig)
      .filter(
        (settingsCode) =>
          onlinePersistanceConfig[settingsCode].retrieve !== undefined
      )
      .map((settingsName) => {
        return (async () => {
          let value = await onlinePersistanceConfig[settingsName].retrieve(
            context
          )
          onlineSettings[settingsName] = value
        })()
      })
  )
  /*console.log('getSettingsParamFromRemote', onlineSettings)*/
  return onlineSettings
}
