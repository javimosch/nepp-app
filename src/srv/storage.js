import { createLocalStorage } from '@/srv/cache.js'

const localStorage = {
  ...createLocalStorage(),
  fromNamespace(namespacePrefix) {
    return createLocalStorage(namespacePrefix)
  },
}


export default localStorage
