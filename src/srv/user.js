import api from '@/srv/api'
import Ajv from 'ajv'

const ajv = new Ajv({
  allErrors: true,
})
ajv.addKeyword('$isNotEmpty', {
  type: 'string',
  validate: function (schema, data) {
    return typeof data === 'string' && data.trim() !== ''
  },
  errors: false,
})

export const normalizedUserParametersTable = {
  maxvue: 'userMaxMapPredefinedViewsCount',
  zoom_level: 'defaultMapZoomLevel',
  baselayer: 'userBaseLayer',
}

/**
 * Used by Settings store.
 *
 * @param {*} currentUserLogin
 * @returns {Object} Settings state object
 * Example:
 * {
 *  defaultMapZoomLevel: 14,
 *  userBaseLayer: cBaseLayerSabatier
 * }
 */
export async function getUserParametersAsObject(
  currentUserLogin = '',
  userParams = null
) {
  let result = {}
  userParams = userParams || (await getUserParameters(currentUserLogin))
  userParams.forEach((userParam) => {
    if (normalizedUserParametersTable[userParam.originalCode] !== undefined) {
      result[userParam.code] = userParam.value
    }
  })
  /*console.log('getUserParametersAsObject', {
    currentUserLogin,
    userParams,
    result,
  })*/
  return result
}

export async function getUserParameters(currentUserLogin = '') {
  let params = await api.v3.get(
    api.APIUrls.APIV3_GEORED_CLIENT_USER_PARAMETERS,
    {
      'user.login': currentUserLogin,
    }
  )
  let filteredUserParams = (params.data || [])
    .filter((param) => {
      if (
        currentUserLogin &&
        !!param.userLogin &&
        param.userLogin.toLowerCase() !== currentUserLogin.toLowerCase()
      ) {
        return false //Filter out other users parameters if currentUserLogin is provided and userLogin is available in the API response.
      }
      return true
    })
    .map(function normalizeUserParameter(param) {
      return {
        id: param.id,
        originalCode: param.parameter,
        code: normalizedUserParametersTable[param.parameter] || param.parameter,
        value: param.value,
      }
    })
  /*console.log('getUserParameters', {
    filteredUserParams,
  })*/
  return filteredUserParams
}

export async function updateUserParameter(values = {}) {
  if (values.value) {
    values.value = values.value.toString()
  }
  /*console.log('updateUserParameter', {
    values,
  })*/
  const isValid = ajv.compile({
    type: 'object',
    properties: {
      id: { type: 'number', $isNotEmpty: true },
      parameter: { type: 'string', $isNotEmpty: true },
      value: { type: 'string', $isNotEmpty: true },
    },
    required: ['parameter', 'value', 'id'],
  })

  ///console.log('updateUserParameter', values)

  //Convert settings name (Normalized) into user parameter name
  if (Object.values(normalizedUserParametersTable).includes(values.parameter)) {
    //i.g maxvue instead of userMaxMapPredefinedViewsCount
    values.parameter = Object.keys(normalizedUserParametersTable).find(
      (k) => normalizedUserParametersTable[k] === values.parameter
    )
  }

  //console.log('updateUserParameter::normalized', values.parameter, values.value)

  if (!isValid(values)) {
    console.error({
      errors: ajv.errorsText(isValid.errors),
    })
    throw new Error(ajv.errorsText(isValid.errors))
  }

  await api.v3.patch(
    `${api.APIUrls.APIV3_GEORED_CLIENT_USER_PARAMETERS}/${values.id}}`,
    values
  )
  return true
}
