import { apiPlatformResourcesConfiguration } from '@/cfg/apis.js'
import * as R from 'ramda'
const debug = false
const defaultPoolingItemsPerRequest = 500

async function transformPayloadToAPIPlatformFormat(resourceName, payload = {}) {
  try {
    let mod = await getNormalizeModule(resourceName)
    if (mod.mutateNormalize) {
      payload = mod.mutateNormalize(payload)
      debug &&
        console.log('transformPayloadToAPIPlatformFormat', {
          payload,
        })
    }
  } catch (err) {
    console.log(err.stack)
  }
  return payload
}

export function useApiPlatformResources({ apiWrapper, getAPIV3Pooling }) {
  let apiResources = {}
  for (let resourceName of Object.keys(apiPlatformResourcesConfiguration)) {
    let resourcePath = apiPlatformResourcesConfiguration[resourceName]
    apiResources[resourceName] = {
      async create(values = {}) {
        if (values.id) {
          return apiResources[resourceName].edit(values)
        }

        values = await transformPayloadToAPIPlatformFormat(resourceName, values)

        let r = await apiWrapper.v3.post(resourcePath, values)
        debug &&
          console.log(`${resourceName} create `, {
            r,
          })
        return await normalizeResourceApiResponse(resourceName, r.data || r)
      },
      async edit(values = {}) {
        values = await transformPayloadToAPIPlatformFormat(resourceName, values)
        if (!values.id) {
          throw new Error('id property expected')
        }
        let r = await apiWrapper.v3.patch(
          `${resourcePath}/${values.id}`,
          R.omit(['id'], values)
        )
        let normalizedRes = await normalizeResourceApiResponse(
          resourceName,
          r.data || r
        )
        debug &&
          console.log(`${resourceName} edit `, {
            r,
            normalizedRes,
          })
        return normalizedRes
      },
      async get(id) {
        if (!id) {
          throw new Error('id required')
        }
        let r = await apiWrapper.v3.get(`${resourcePath}/${id}`)
        debug &&
          console.log(`${resourceName} edit `, {
            r,
          })
        return await normalizeResourceApiResponse(resourceName, r.data || r)
      },
      async getAll(query = {}, options = {}) {
        let r = await apiWrapper.v3.get(resourcePath, query, options)
        return await normalizeResourceApiResponse(resourceName, r.data || r)
      },
      /**
       *
       * @param {Boolean} options.freezeItems Applies Object.freeze() to all items
       * @returns
       */
      async getAllPooling(options = {}) {
        //Inject normalization
        let callback = !options.callback
          ? null
          : async (items) => {
              return options.callback(
                await normalizeResourceApiResponse(resourceName, items, options)
              )
            }

        debug && console.log('getAllPooling:start', { options })

        let r = await getAPIV3Pooling({
          uri: `${resourcePath}?page=1&itemsPerPage=${
            options.poolingItemsPerRequest || defaultPoolingItemsPerRequest
          }`,
          ...options,
          callback,
        })

        debug && console.log('getAllPooling:end', { options, r })

        return await normalizeResourceApiResponse(
          resourceName,
          r.data || r,
          options
        )
      },
      async delete(id) {
        if (!id) {
          throw new Error('id required')
        }
        let r = await apiWrapper.v3.delete(`${resourcePath}/${id}`)
        return r.data || r
      },
    }

    apiResources[resourceName].normalizeItem = (r) => r
    getNormalizeItemHandler(resourceName).then((handler) => {
      apiResources[resourceName].normalizeItem =
        handler || apiResources[resourceName].normalizeItem
    })
  }
  /*debug &&
    console.log(
      `The following api platform resources are available: `,
      Object.keys(apiResources).join(', ')
    )*/
  window.apiResources = apiResources
  return apiResources
}

function getNormalizeItemHandler(resourceName) {
  return new Promise((resolve, reject) => {
    import(`./normalization/${resourceName}.js`)
      .then((module) => {
        //debug && console.log('DYNAMIC', module)
        resolve(typeof module.default === 'function' ? module.default : null)
      })
      .catch((err) => {
        resolve(null)
      })
  })
}
function getNormalizeModule(resourceName) {
  return new Promise((resolve, reject) => {
    import(`./normalization/${resourceName}.js`)
      .then((module) => {
        resolve(module || null)
      })
      .catch((err) => {
        resolve(null)
      })
  })
}

function normalizeResourceApiResponse(resourceName, response, options = {}) {
  return new Promise((resolve, reject) => {
    if (typeof response !== 'object' && !(response instanceof Array)) {
      debug &&
        console.log('normalizeResourceApiResponse::skip', {
          response,
        })
      return resolveWithOptions(response)
    }

    function resolveWithOptions(res) {
      if (options.sort && res.sort) {
        res = res.sort(options.sort)
      }
      resolve(res)
    }

    function applyOptionsToSingleItem(item) {
      if (options.freezeItems) {
        item = Object.freeze(item)
      }
      return item
    }

    getNormalizeItemHandler(resourceName)
      .then((handler = null) => {
        if (!handler) {
          return resolveWithOptions(response)
        }

        if (response instanceof Array) {
          resolveWithOptions(
            response.map((item) =>
              applyOptionsToSingleItem(handler(item, options))
            )
          )
        } else {
          resolveWithOptions(
            applyOptionsToSingleItem(handler(response, options))
          )
        }
      })
      .catch((err) => {
        console.error(err)
        reject(err)
      })
  })
}
