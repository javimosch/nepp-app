import { getQueryStringValue } from '@/lib/querystring'
import { isUnitTest } from '@/lib/unit.js'
/**
 * Some frameworks prefix variables differently
 */
export const envNames = {
  VUE_APP_ROUTER_DEFAULT_ROUTE: 'VUE_APP_ROUTER_DEFAULT_ROUTE',
  VUE_APP_API_GEORED_HOST: 'VUE_APP_API_GEORED_HOST',
  VUE_APP_RELEASE_NAME: 'VUE_APP_RELEASE_NAME',
  VUE_APP_RELEASE_TIMESTAMP: 'VUE_APP_RELEASE_TIMESTAMP',
  VUE_APP_DISABLE_USER_AUDIT: 'VUE_APP_DISABLE_USER_AUDIT',
  VUE_APP_FILTERS_MAX_SELECTION: 'VUE_APP_FILTERS_MAX_SELECTION',
  VUE_APP_FILTERS_MAX_SELECTION_DATES: 'VUE_APP_FILTERS_MAX_SELECTION_DATES',
  VUE_APP_IDENTIFICATION_MODULE_SEARCH_SELECTION_LIMIT:
    'VUE_APP_IDENTIFICATION_MODULE_SEARCH_SELECTION_LIMIT',
  VUE_APP_SEARCH_SELECTION_LIMIT: 'VUE_APP_SEARCH_SELECTION_LIMIT',
  VUE_APP_CLIENT_PARAMETERS_URI: 'VUE_APP_CLIENT_PARAMETERS_URI',
  VUE_APP_GOOGLE_GEOCODING: 'VUE_APP_GOOGLE_GEOCODING',
  VUE_APP_AUTOCOMPLETE_DEFAULT_PROVIDER:
    'VUE_APP_AUTOCOMPLETE_DEFAULT_PROVIDER',
  VUE_APP_GEORED_HOST: 'VUE_APP_GEORED_HOST',
  VUE_APP_CITIFRET_HOST: 'VUE_APP_CITIFRET_HOST',
  VUE_APP_CITIPAV_HOST: 'VUE_APP_CITIPAV_HOST',
  VUE_APP_GOOGLE_KEY: 'VUE_APP_GOOGLE_KEY',
  VUE_APP_APP_GEONLINE_NEWS_API_HOST_URL:
    'VUE_APP_APP_GEONLINE_NEWS_API_HOST_URL',
  VUE_APP_AUTOREFRESH_INTERVAL: 'VUE_APP_AUTOREFRESH_INTERVAL',
  events_enable_group_all: 'VUE_APP_EVENTS_CAN_GROUP_EVENTS_BY_ALL',

  IFRAME_WHITELIST_URLS: 'VUE_APP_IFRAME_WHITELIST_URLS',

  VUE_APP_LASTPASSEDVEHICLES_FEATURE_SEARCH_SELECTION_LIMIT:
    'VUE_APP_LASTPASSEDVEHICLES_FEATURE_SEARCH_SELECTION_LIMIT',
  VUE_APP_MESSAGES_FEATURE_SEARCH_SELECTION_LIMIT:
    'VUE_APP_MESSAGES_FEATURE_SEARCH_SELECTION_LIMIT',
}

const customEnvs = {
  VUE_APP_RELEASE_NAME:
  import.meta.env.RELEASE_NAME || import.meta.env.VUE_APP_RELEASE_NAME,
  VUE_APP_RELEASE_TIMESTAMP:
    import.meta.env.RELEASE_TIMESTAMP || import.meta.env.VUE_APP_RELEASE_TIMESTAMP,
  //Test sentry locally
  //VUE_APP_SENTRY_DSN:"https://0389040e65d44560b9fe03454743f432@sentry.geosab.eu/4"
}

const envService = {
  isTestMode,
  getDevOnlyQueryStringValue,
  getEnvValue,
  envs: import.meta.env,
  isLocalhost,
  isDev,
  isRecette,
  isIsoprod,
  isBeta,
  isPreprod,
  getEnvName() {
    if (this.isDev()) return 'dev'
    if (this.isRecette()) return 'recette'
    if (this.isIsoprod()) return 'isoprod'
    if (this.isBeta()) return 'beta'
    if (this.isPreprod()) return 'preprod'
    return 'production'
  },
  isProduction
}

/**
   * @function isLocalhost
   * @alias $env.isLocalhost
   * @public
   * @returns {boolean}
   */
function isLocalhost() {
  return window.location.origin.indexOf('localhost') !== -1
}
/**
 * @function isDev
 * @alias $env.isDev
 * @public
 * @returns {boolean}
 */
function isDev() {
  return (
    window.location.origin.indexOf('dev-') !== -1 ||
    (this.getEnvValue('VUE_APP_API_GEORED_HOST') || '').indexOf('dev-') !== -1
  )
}
function isRecette() {
  return (
    window.location.origin.indexOf('recette-') !== -1 ||
    (this.getEnvValue('VUE_APP_API_GEORED_HOST') || '').indexOf(
      'recette-'
    ) !== -1
  )
}
function isIsoprod() {
  return (
    window.location.origin.indexOf('isoprod-') !== -1 ||
    (this.getEnvValue('VUE_APP_API_GEORED_HOST') || '').indexOf(
      'isoprod-'
    ) !== -1
  )
}
function isBeta() {
  return (
    window.location.origin.indexOf('beta-') !== -1 ||
    (this.getEnvValue('VUE_APP_API_GEORED_HOST') || '').indexOf('beta-') !==
      -1
  )
}
function isPreprod() {
  return (
    window.location.origin.indexOf('preprod-') !== -1 ||
    (this.getEnvValue('VUE_APP_API_GEORED_HOST') || '').indexOf(
      'preprod-'
    ) !== -1
  )
}

/**
* @function isProduction
* @alias $env.isProduction
* @public
* @returns {boolean}
*/
function isProduction() {
 return (
   (isUnitTest ||
     (!isLocalhost() &&
       !isDev() &&
       !isRecette() &&
       !isIsoprod() &&
       !isPreprod())) &&
   import.meta.env.NODE_ENV === 'production'
 )
}

/**
 * Wrapper to retrieve env values. Adapt to different frameworks if needed (i.g vite)
 * @param {*} name
 * @function getEnvValue
 * @param {Function} options.transform Transform value before return
 * @returns {String}
 */
export function getEnvValue(name, defaultValue = '', options = {}) {
  let computedName = envNames[name] || name
  let value = ''
  //On dev envs, envs can be set via querystring
  if (
    !computedName.includes('GEORED_HOST') &&
    !isProduction() &&
    (getQueryStringValue(computedName).length >= 1 ||
      getQueryStringValue(name).length >= 1)
  ) {
    value = getQueryStringValue(computedName) || getQueryStringValue(name)
  } else {
    let fallBackValue = computedName.toUpperCase().includes("VUE_APP") ? import.meta.env[computedName.split('VUE_APP').join('VITE_APP')]||defaultValue : defaultValue
    value =
      import.meta.env[computedName] || customEnvs[computedName] || fallBackValue || defaultValue
  }

  if (options.transform) {
    return options.transform(value)
  }
  return value
}

export function getDevOnlyQueryStringValue(name) {
  if (!isProduction()) {
    return getQueryStringValue(name)
  } else {
    return ''
  }
}

export function isTestMode() {
  return getDevOnlyQueryStringValue('test') === '1'
}

export function isVerboseMode(str = '1') {
  return (
    (getQueryStringValue('verbose') || '').includes('1') ||
    (getQueryStringValue('verbose') || '').includes(str)
  )
}


export default envService
