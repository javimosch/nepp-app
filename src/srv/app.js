import envService from '@/srv/env.js'
import moment from 'moment'

export default {
  /**
   * @function getAppVersion
   * @returns {String}
   */
  getAppVersion() {
    let str = ''
    if (envService.getEnvValue('VUE_APP_RELEASE_NAME')) {
      let v = envService.isProduction() || envService.isPreprod() ? 'v' : ''
      str += `${v}${envService.getEnvValue('VUE_APP_RELEASE_NAME')}`
    }
    if (envService.getEnvValue('VUE_APP_RELEASE_TIMESTAMP')) {
      str += `-${moment
        .unix(envService.getEnvValue('VUE_APP_RELEASE_TIMESTAMP'))
        .format('DDMMYYYY')}`
    }
    str = str || envService.getEnvName() //Fallback to env name (dev,isoprod, etc)
    return str
  },
  /**
   * Normally 12 (Tsection ID)
   * @returns {String}
   */
  getAppIdentifier() {
    return envService
      .getEnvValue('VUE_APP_GEOREDV3_IDENTIFIER', '12')
      .toString()
  },
}
