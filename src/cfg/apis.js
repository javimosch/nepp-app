import { getEnvValue } from '@/srv/env.js'

/**
 * @todo Use these variables instead of hardcoded URLs
 * @todo Rename with prefix to indicate method. Example: APIV2_POST, APIV3_PATCH
 */
const APIUrls = {
  APIV3_GET_POSITION: '/positions/geored/get_position',
  APIV3_GET_POSITIONS: '/positions/geored/get_positions',
  APIV2_POSITIONS_GPS: '/v2/historique/positions_gps',
  APIV2_POSITIONS_CAPTEURS: '/v2/historique/positions_capteurs',
  APIV3_GET_SENSORS: '/positions/geored/get_sensors',
  APIV2_POSITIONS: '/v2/historique/positions',
  APIV2_HISTORIQUE_BY_VEHICLE_DATES: '/v2/historique/by_vehicule_dates',
  APIV2_HISTORIQUE_TRONCONS_DETAILS: '/v2/historique/troncons_details',
  APIV2_HISTORIQUE_CIRCUIT_TRONCONS: '/v2/historique/circuit_troncons',
  APIV2_HISTORIQUE_DERNIER_CIRCUIT: '/v2/historique/dernier_circuit',
  APIV2_HISTORIQUE_BY_CIRCUIT_DATES: '/v2/historique/by_circuit_dates',
  APIV2_HISTORIQUE_CONSOMMATION_PAR_PORTION:
    '/v2/historique/consommation_par_portion',
  APIV2_EVENEMENTS_CLIENT_INDICATEURS: '/v2/evenements_client/indicateurs.json',
  APIV3_AGGREGATE_EVENTS: '/geored/events/aggregate_events',
  APIV3_EVENTS_ANOMALIES: '/geored/events/anomalies',
  APIV2_ZONE_CLIENT: '/v2/zoneclient',
  APIV2_MESSAGE_PREDEFINI_GET: '/v2/message_predefini/get',
  APIV2_IDENTIFICATION_LEVEE_BAC_DETAIL:
  import.meta.env.VUE_APP_API_IDENTIFICATION_BAC_DETAILS_URI ||
    '/v2/levee_bac_detail',
  APIV3_VEHICLE_DETAILS: '/geored/vehicle/vehicles',
  APIV3_VEHICLE: '/geored/vehicle/vehicles',
  APIV3_POSITIONS_CUMULATION: '/positions/geored/cumulation',
  APIV3_EVENTS_CUMULATION: '/geored/events/service/cumulation',
  APIV3_VEHICLE_BOX_ALLOCATIONS: '/geored/vehicle/box_allocations',
  APIV3_BOX_SENSOR_ASSIGNMENTS: '/geored/box/sensor_assignments',
  APIV3_BOX_CONFIGURATIONS: '/geored/box/box_configurations',
  APIV3_CLIENT_LOGOS: getEnvValue(
    'VUE_APP_CLIENT_LOGO_URI',
    '/geored/client/logos'
  ),
  APIV3_CLIENT_PARAMETERS: getEnvValue(
    'VUE_APP_CLIENT_PARAMETERS_URI',
    '/geored/client_parameters'
  ),
  APIV3_USER_ACTIVITY: '/geored/service/client/get_user_activity',
  APIV3_USER_LOGIN_AS: '/geored/service/user/login_as',
  APIV3_CLIENT_HIRERARCHIES: '/geored/client/hierarchies',
  APIV2_MESSAGE_ACK: '/v2/cdm/message_alerte/ack',
  APIV2_CDM_TYPE_ALERTE: '/v2/cdm/type_alerte',
  APIV2_CHAUFFEUR_CATEGORIES: '/v2/chauffeur/categorie',
  APIV2_CHAUFFEURS: `/v2/chauffeur`,
  APIV2_CIRCUIT_CATEGORIES: `/v2/circuit/categorie`,
  APIV2_CIRCUITS: `/v2/circuits`,
  APIV2_MESSAGES_ALERTES: '/v2/cdm/messages_alertes',
  APIV2_TEMP_REEL_INDICATEUR_BY_VEHICLE:
    '/v2/temps_reel/indicateurs/by_vehicule.json',
  APIV2_IDENTIFICATION_LEVEE_BAC: '/v2/levee_bac',
  //APIV2_MOB_CATEGORIES_VEHICLES: '/v2/mob_categoriesvehicules',
  //APIV2_MOB_VEHICLES: `/v2/mob_vehicules`,
  APIV3_JAVASCRIPT_ERRORS: '/javascript_errors',
  APIV3_CARTO_GEOCODING_PROXY_OSM_NOMINATIM:
    '/service/cartography/osm_nominatim',
  APIV3_CARTO_GEOCODING_PROXY_GOOGLE: getEnvValue(
    'VUE_APP_GOOGLE_GEOCODING',
    '/service/cartography/google'
  ),
  APIV3_CARTO_GEOCODING_NATURAL: '/service/cartography/natural_geocoding',
  APIV3_CARTO_GEOCODING: '/service/cartography/geocoding',
  APIV3_CARTO_GEOCODING_REVERSE:
  import.meta.env.VUE_APP_REVERSE_GEOCODING_URI ||
    '/service/cartography/reverse_geocoding',
  APIV3_CARTO_ROUTING:
  import.meta.env.VUE_APP_ROUTING_GEOCODING_URI || '/service/cartography/routing',
  APIV3_USER_SESSIONS: '/geored/client/user/sessions',
  APIV3_USER_AUDITS: '/geored/client/user/audits',
  APIV3_GET_BIN_COLLECTION_BLACK_LIST_HISTOS:
    '/bin_collection/black_list_histos',
  APIV3_CLIENT_CARTOGRAPHY: '/geored/client/clients/${clientId}/cartography',
  APIV3_USER_ACCESS_RIGHTS: '/geored/client/user_access_rights',
  APIV3_ECO_DRIVING_VEHICLES: '/eco_driving_vehicles/aggregate_client',
  APIV3_ECO_DRIVING_DRIVERS: '/eco_driving_drivers/aggregate_client',
  APIV3_ECO_DRIVING_CIRCUITS: '/eco_driving_rounds/aggregate_client',
  APIV2_ECOCONDUITE_ECO_DRIVING_VEHICLES:
    '/v2/eco_conduite/eco_driving_vehicles',
  APIV2_ECOCONDUITE_ECO_DRIVING_DRIVERS: '/v2/eco_conduite/eco_driving_drivers',
  APIV2_ECOCONDUITE_ECO_DRIVING_CIRCUITS: '/v2/eco_conduite/eco_driving_rounds',
  APIV3_SERVICE_VEHICLE_CONFIGURATION: '/geored/vehicle/service/configuration',
  APIV3_VEHICLE_CATEGORIES: '/geored/vehicle_categories',
  APIV3_DRIVER_CATEGORIES: '/geored/driver_categories',
  APIV3_DRIVERS: '/geored/drivers',
  APIV3_ROUND_CATEGORIES: `/geored/round_categories`,
  APIV3_ROUNDS: `/geored/rounds`,
  APIV2_TEMPS_REEL_BY_VEHICLE: '/v2/temps_reel/by_vehicule',
  APIV2_TEMPS_REEL_BY_DRIVER: '/v2/temps_reel/by_chauffeur',
  APIV2_HISTORIQUE_BY_DRIVER_DATES: '/v2/historique/by_chauffeur_dates',
  APIV2_TEMPS_REEL_BY_CIRCUIT: '/v2/temps_reel/by_circuit',
  APIV3_GET_GEORED_CLIENT_USER_MAP_VIEWS: '/geored/client/user/map_views',
  APIV3_POST_GEORED_CLIENT_USER_MAP_VIEWS: '/geored/client/user/map_views',
  APIV3_GEORED_ROUND_VERSIONS: '/geored/round/versions',
  APIV3_GEORED_ROUND_VERSIONS_SECTIONS: '/geored/round/versions_sections',
  APIV3_GEORED_ROUND_ACTIVITIES: '/geored/round/activities',
  APIV3_GEORED_ROUND_SERVICE_LAST_ROUND_EXECUTION_SECTIONS:
    '/geored/round/service/last_round_execution_sections',
  APIV3_GEORED_ROUND_MANEUVERS: '/geored/round/maneuvers',
  APIV3_GEORED_CLIENT_USER_PARAMETERS: '/geored/client/user_parameters',
  APIV3_SERVICE_NEARBY_VEHICLES: '/service/cartography/nearby_vehicles',
  APIV3_GEORED_REALTIME_SERVICE_TO_KML: '/geored/real_time/service/to_kml',
  APIV3_GEORED_SERVICE_HISTORY_TO_KML: '/geored/service/history/to_kml',
  APIV3_ZONE_TYPES: '/geored/area/area_types',
  APIV3_ZONES: '/geored/area/areas',
  APIV3_ZONE_CATEGORIES: '/geored/area_categories',
  APIV3_CLIENT_SERVICE_CARTOGRAPHY: '/geored/client/service/cartography',
  APIV3_GEORED_SERVICE_HISTORY_TO_SHAPE: '/geored/service/history/to_shape',
  APIV3_GEORED_SERVICE_ROUND_EXECUTION_PENDULUM:
    '/geored/round/service/round_execution/${roundExecutionId}/pendulum',
  APIV3_GEORED_SERVICE_HISTORY_TO_ROUND:
    '/geored/service/history/to_round/${vehicleId}',
  APIV3_GEORED_ROUND_CALCULATION_METHODS: '/geored/round/calculation_methods',
  APIV3_GEORED_SERVICE_CARTOGRAPHY_LAST_VEHICLES:
    '/service/cartography/last_vehicles',
  APIV3_GEORED_SERVICE_HISTORY_VEHICLES: '/geored/service/history/vehicles',
  APIV3_GEORED_SERVICE_HISTORY_SECTIONS: '/geored/service/history/sections',
  APIV3_GEORED_SERVICE_VEHICLE_MESSAGES:
    '/geored/message/service/vehicle-messages',
  APIV3_GEORED_MESSAGE_SERVICE_SEND_MESSAGE:
    '/geored/message/service/send-messages',
  APIV3_GEORED_MESSAGE_PREDEFINED: '/geored/message/predefineds',
  APIV3_GEORED_MESSAGE_PLANNING: '/geored/message/message_plannings',
  APIV3_GEORED_RECURRENCE_RECURRENCE_DAYS: '/geored/recurrence/recurrence_days',
  APIV3_GEORED_RECURRENCE_RECURRENCE_FREQUENCIES:
    '/geored/recurrence/recurrence_frequencies',
  APIV3_GEORED_MESSAGE_SERVICE_SEND_PLANNED_MESSAGE:
    '/geored/message/service/send-planning-messages',
  APIV3_GEORED_MESSAGE_VEHICLE_MESSAGE_PLANNING:
    '/geored/message/vehicle_message_plannings',

  APIV2_PUBLIC_AUTHENTIFICATION: '/public/authentification'
}
export default APIUrls

/**
 * @todo Replace hardcoded moment format with these variables in the entire application
 */
export const APIV3RequestDatetimeFormat = 'YYYY-MM-DDTHH:mm:ss' //[Z]
export const APIV2RequestDatetimeFormat = 'YYYY-MM-DD HH:mm:ss'
export const APIV2RequestDateFormat = 'YYYY-MM-DD'
export const APIV2ResponseDatetimeFormat = 'YYYY[-]MM[-]DD HH:mm:ss'
export const APIV2RequestDatetimeFormatWithCustomTime = (time = '00:00:00') =>
  `${APIV2RequestDatetimeFormat.split('HH:mm:ss').join(time)}`

export const apiPlatformResourcesConfiguration = {
  zoneType: APIUrls.APIV3_ZONE_TYPES,
  zone: APIUrls.APIV3_ZONES,
  serviceCartography: APIUrls.APIV3_CLIENT_SERVICE_CARTOGRAPHY,
  calculationMethod: APIUrls.APIV3_GEORED_ROUND_CALCULATION_METHODS,
  activity: APIUrls.APIV3_GEORED_ROUND_ACTIVITIES,
  lastVehicleSrv: APIUrls.APIV3_GEORED_SERVICE_CARTOGRAPHY_LAST_VEHICLES,
  historyVehicleSrv: APIUrls.APIV3_GEORED_SERVICE_HISTORY_VEHICLES,
  historySectionsSrv: APIUrls.APIV3_GEORED_SERVICE_HISTORY_SECTIONS,
  vehicleMessagesSrv: APIUrls.APIV3_GEORED_SERVICE_VEHICLE_MESSAGES,
  predefinedMessage: APIUrls.APIV3_GEORED_MESSAGE_PREDEFINED,
  plannedMessage: APIUrls.APIV3_GEORED_MESSAGE_PLANNING,
  recurrenceDay: APIUrls.APIV3_GEORED_RECURRENCE_RECURRENCE_DAYS,
  recurrenceFrequency: APIUrls.APIV3_GEORED_RECURRENCE_RECURRENCE_FREQUENCIES,
  vehiclePlannedMessage: APIUrls.APIV3_GEORED_MESSAGE_VEHICLE_MESSAGE_PLANNING,
}
