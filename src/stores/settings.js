import { defineStore } from 'pinia'
import { reactive, computed } from 'vue'
import { getQueryStringValue } from '@/lib/querystring'
import { getClientLogoAsBase64 } from '@/srv/auth'
import {
    saveSettingsParamLocally,
    saveSettingsParamRemotely,
    getSettingsParamFromRemote,
} from '@/srv/settings.js'
import i18n, { isValidLocaleCode } from '@/srv/i18n'
import storage from '@/srv/storage.js'
import {
    getUserParameters,
    getUserParametersAsObject,
} from '@/srv/user.js'
import { useAuthStore } from '@/stores/auth'

//Feat: Skip cache for certain settings
let cacheBlacklist = ['osmLayers']

export const useSettingsStore = defineStore('settings', () => {
    const authStore = useAuthStore()

    const state = reactive({
        parameters: newState()
    })

    function newState() {
        return {
            //Used to PATCH settings parameters (thanks to ids)
            userParameters: [],

            hasSyncFromCache: false,

            clientLogoBase64: '',

            //Language will be decided during login action (default to fr)
            applicationLanguage: '',

            //Cartography

            //-- Predefined views
            //---- Default predefined view
            defaultMapPredefinedView: null,
            //-- Carto
            //---- Default zoom level
            defaultMapZoomLevel: null,
            //---- Zones
            //------ Client zones
            areClientZonesEnabledOnTheMap: false,
            //------ Client zone types
            enabledClientZones: [],
            clientBasemapItems: [],

            naturalGeocodingProvider: 'osm',

            //Base maps
            //Selected Leaflet base map
            wmsItem: {},
            googleLayers: [],
            areOSMLayersDisabled: false,
            osmLayers: [],
            userBaseLayer: null,

            //location
            autorefreshTimeInterval: 2,

            //User parameters (user client-side / database setting)
            userMaxMapPredefinedViewsCount: 999, //database name: maxvue

            //Events (client/user client-side setting)
            isAggregateEventGroupRoundEnabled: false,
            isAggregateEventGroupBinEnabled: false,

            //Zones
            //Zone rendering
            isZoneRenderingGeometryVisible: true,
            isZoneRenderingIconVisible: true,
            isZoneRenderingAcessPointVisible: true,
        }
    }

    console.log('store settings state initial', state)

    window._ss = state

    const getParameter = (name) => state.parameters[name]

    const setParameters = (parameters = {}) => {
        console.log('store settings setParameters', {
            parameters: state.parameters,
            input: parameters
        })
        state.parameters = {
            ...state.parameters,
            ...parameters
        }
    }

    async function setParameterAndSave({ name, value }) {
        await setParameter({ name, value })
        await saveParameter(name)
    }

    async function saveParameters() {
        await Promise.all(
            Object.keys(state.parameters).map((stateKey) => {
                return (async () => {
                    if (authStore.isLogged) {
                        await saveSettingsParamRemotely(
                            stateKey,
                            state.parameters[stateKey],
                            authStore.loginAs.value,
                            state.parameters.userParameters
                        )

                        console.log(`store settings saveParameters key ${stateKey}`, {
                            value: state.parameters[stateKey]
                        })

                        await saveSettingsParamLocally(
                            getCacheKey(
                                stateKey,
                                authStore.loginNameClientIdEncoded.value
                            ),
                            state.parameters[stateKey] 
                        )
                    }
                })()
            })
        )
    }

    function setParameter({ name, value }) {
        console.log('store settings setParameter', {
            name,
            value
        })
        setParameters({
            [name]: value,
        })
    }

    function saveParameter(name) {
        console.log('store settings saveParameter', {
            value: state.parameters[name]
        })
        saveSettingsParamLocally(getCacheKey(name), state.parameters[name])
        if (authStore.isLogged.value) {
            saveSettingsParamRemotely(name, state.parameters[name], authStore.isLoginAs.value, state.parameters.userParameters)
        }
    }

    async function syncFromServer() {
        const currentUserLogin = authStore.loginName.value
        let userParameters = Object.freeze(await getUserParameters(currentUserLogin))

        setParameters({
            ...state.parameters,
            userParameters,
            clientLogoBase64: await getClientLogoAsBase64('default'),

            //The language choice at login view has priority over the client language
            applicationLanguage:
                state.parameters.applicationLanguage ||
                getComputedLanguageCodeFromCountryCode(
                    authStore.clientCountry.value
                ) ||
                'fr',


            //Retrieve user parameters and merge them into state (userMaxMapPredefinedViewsCount, defaultMapZoomLevel, userBaseLayer)
            ...(await getUserParametersAsObject(
                currentUserLogin,
                state.parameters.userParameters
            )),

            //Retrieve other parameters from online persistance (defaultMapPredefinedView)
            ...(await getSettingsParamFromRemote({
                isLoginAs: authStore.isLoginAs.value,
            })),
        })

        saveParameters()
    }



    async function syncFromCache() {
        const defaultState = newState()
        const keyPart = '_' + authStore.loginNameClientIdEncoded.value
        const data = {}
        const keys = await storage.keys()
        const cachedState = {}
        await Promise.all(
            Object.keys(state.parameters)
                .filter((stateKey) => !cacheBlacklist.includes(stateKey))
                .map((stateKey) => {
                    return (async () => {
                        let includesKey =
                            keys.findIndex((key) => key.includes(stateKey)) !== -1

                        let value = null

                        if (includesKey) {
                            value = await storage.getItem(stateKey + keyPart)

                            //Fallback/Hot-fix: Old version adds a '}' at the end of the cache key (bug)
                            //Proceed to the fallback value only if value equals null
                            value =
                                value !== null
                                    ? value
                                    : await storage.getItem(stateKey + keyPart + '}')
                        }

                        cachedState[stateKey] = value
                        data[stateKey] = value !== null ? value : defaultState[stateKey]
                    })()
                })
        )
        data.hasSyncFromCache = true
        setParameters(data)
    }

    return {
        parameters: state.parameters,
        setParameter,
        getParameter,
        saveParameter,
        syncFromServer,
        syncFromCache,
        setParameterAndSave
    }
})
export default useSettingsStore


export function getComputedLanguageCodeFromCountryCode(countryCode) {
    let countryCodeNormalized = (countryCode || '').toString().toLowerCase()
    return isValidLocaleCode(countryCodeNormalized) ? countryCodeNormalized : ''
}


function getCacheKey(stateKey = '', segregationKey = '') {
    return `${stateKey}_${segregationKey}`
}