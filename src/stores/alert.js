import { defineStore } from 'pinia'
import {reactive, computed} from 'vue'
import { generateShortId } from '@/lib/crypto.js'

export const useAlertStore = defineStore('alert',()=> {
  const state = reactive({
    alerts:[]
  })
  const setAlerts = alerts => state.alerts = alerts

  function addAlert(alert) {
    const id = generateShortId()
    alert.id = id
    setAlerts([...state.alerts, alert])
    if (alert.timeout) {
      setTimeout(() => {
        removeAlert(id)
      }, alert.timeout)
    }
  }

  function removeAlert(id) {
    const index = state.alerts.findIndex((alert) => alert.id === id)
    if (index !== -1) {
      setAlerts(state.alerts.splice(index, 1))
    }
  }

  return {
    state,
    alerts:state.alerts,
    addAlert,
    removeAlert,
  }
})
export default useAlertStore