import { saveSettingsParamLocally } from '@/srv/settings'
import api from '@/srv/api'
import * as rightsSrv from '@/srv/rights'
import { isTestMode } from '@/srv/env.js'
import APIUrls from '@/cfg/apis.js'
import cache from '@/lib/cache'
import { defineStore } from 'pinia'
import { computed, reactive } from 'vue'
import jwt_decode from 'jwt-decode'
import useAlertStore from '@/stores/alert'
import useSettingsStore from '@/stores/settings'
import moment from 'moment'
import { getQueryStringValue } from '@/lib/querystring'
import storage from '@/srv/storage.js'
import auditSrv from '@/srv/audit'
import { getClientParameter, encodeLoginNameClientId } from '@/srv/auth'

const shouldLog =
  (getQueryStringValue('verbose') || '').includes('1') ||
  (getQueryStringValue('verbose') || '').includes('auth')
const apiStorage = storage.fromNamespace('api')

export const useAuthStore = defineStore('auth', () => {
  const alertStore = useAlertStore()
  const settingsStore = useSettingsStore()

  const state = reactive({
    user_logged: false,
    user_infos: {
      login: '',
      client_nom: '',
      client_id: '',
      /**
       * main jwt (tokens containing "to_client" can't be here)
       */
      token: '', //Will be used by api wrapper if toClientToken is empty
      updated_at: '',
      userId: '',
      /**
       * child token (tokens containing "to_client" token must be here)
       */
      toClientToken: '', //Will be used by api wrapper by default (fallbacks to token property)
      toClientId: '',
      hasChildClients: false,
      country: '', //client country code (i.g FR)
    },
    user_rights: [],
    externalRights: {
      admin: false,
      analyse: false,
      citifret: false,
      citipav: false,
      editour: false, //unused
      simpliboard: false, //unused
    }
  })


  const loginAs = computed(() => {
    let decoded = {}
    try {
      decoded = jwt_decode(state.user_infos.toClientToken)
    } catch (err) {
      decoded = {}
    }
    let toClient = decoded.to_client || ''
    return {
      enabled: !!toClient,
      login: state.user_infos.login,
      client: toClient || clientName.value,
    }
  })

  const isLoginAs = computed(() => loginAs.value.enabled)

  const rightsList = computed(() => state.user_rights)

  const toClientId = computed(() => state.user_infos.toClientId)

  const clientId = computed(() => state.user_infos.client_id)

  const clientName = computed(() => state.user_infos.client_nom)

  const loginName = computed(() => state.user_infos.login)

  const loginNameClientIdEncoded = computed(() => {
    return isLogged.value
      ? encodeLoginNameClientId(
        loginName.value,
        toClientId.value || clientId.value
      )
      : ''
  })

  const isLogged = computed(() => state.user_logged)

  const clientCountry = computed(() => (state.user_infos.country || 'fr').toLowerCase())

  const hasChildClients = computed(() => state.user_infos.hasChildClients)

  const canUseClientSelectionFeature = computed(() => {
    //Use case: V2 (login as child client) --> V3 (Parent token is not passed by, so we are not able to switch client again) (Not implemented)
    if (!state.user_infos.token) {
      return false
    }
    //Has child clients
    return (
      hasChildClients.value ||
      //Or has "Access to login client"
      rightsSrv.hasFeatureRight('common_login_as_access', rightsList.value, loginName.value) ||
      //Or has "Connect as" and is not sabatier
      (clientName.value !== 'sabatier' &&
        rightsSrv.hasFeatureRight('common_login_as_user', rightsList.value, loginName.value))
    )
  })



  function setLogged(infos) {
    infos = infos === null ? {} : infos
    if (Object.keys(infos).length > 0) {
      state.user_infos = Object.assign(
        {},
        {
          login: infos.user || infos.login || '',
          client_id: infos.client_id || infos.clientId || '',
          client_nom: infos.client_nom || infos.client || '',
          token: infos.token,
          updated_at: infos.updated_at || Date.now(),
          userId: infos.userId,
          toClientToken: infos.toClientToken,
          toClientId: infos.toClientId,
          hasChildClients:
            infos.hasChildClients !== undefined
              ? infos.hasChildClients
              : false,
          country: infos.country || '',
        }
      )

      state.user_rights = isTestMode()
        ? infos.rights || []
        : Object.freeze(infos.rights || [])

      state.externalRights = infos.externalRights
      auditSrv.identifyUser({
        id: infos.userId,
      })
      auditSrv.setContext('user_details', {
        client_id: state.user_infos.client_id,
        client_name: state.user_infos.client_nom,
      })
    } else {
      auditSrv.identifyUser(null)
      auditSrv.setContext('user_details', {})
    }
    state.user_logged =
      !!state.user_infos.toClientToken || !!state.user_infos.token
  }

  async function login({ name: login, client, password }) {

    console.log('store auth login', {
      login, client, password
    })

    if (isLogged.value) {
      return
    }

    let account_infos = await api.postFormUrlEncoded(
      api.APIUrls.APIV2_PUBLIC_AUTHENTIFICATION,
      {
        login,
        client,
        password,
      }
    )
    account_infos.data =
      account_infos.data.length !== undefined
        ? account_infos.data[0]
        : account_infos.data

    let isLoginSuccess =
      account_infos && account_infos.data && !!account_infos.data.token

    //shouldLog && console.log({ account_infos: account_infos.data })

    if (!isLoginSuccess) {

      alertStore.addAlert({
        title: '401',
        text: 'alerts.LOGIN_FAIL',
        type: 'warning',
      })


      throw new Error('AUTH_LOGIN_INFOS_FAIL')
    }

    let token_info = await api.postFormUrlEncoded('/public/token', {
      login,
      client,
      password,
    })
    token_info.data =
      token_info.data.length !== undefined
        ? token_info.data[0]
        : token_info.data

    //shouldLog && console.log({ token_info })

    let check_token = await api.post('/public/check_token', {
      token: token_info.data,
    })

    //shouldLog && console.log({ check_token })

    if (!(check_token.data && check_token.data.userId)) {
      throw new Error('AUTH_LOGIN_TOKEN_CHECK_FAIL')
    }

    let infos = await buildUserInfos(account_infos, token_info, check_token)


    if (infos.rights.length === 0) {

      alertStore.addAlert(
        {
          title: '403',
          text: 'alerts.NOT_ENOUGH_RIGHTS',
          type: 'warning',
        }
      )
      throw new Error('AUTH_NO_RIGHTS')
    }

    await authenticateUser(infos)

    shouldLog && console.log('auth login-ok')

    return {
      shouldRouteToClientSelection: canUseClientSelectionFeature.value,
    }
  }

  async function syncLogged(payload) {
    shouldLog &&
      console.log('syncLogged', {
        payload,
      })

    let userInfos = {}

    if (scope.syncInProgress) {
      return new Promise((resolve, reject) => {
        syncLogged().then(resolve).catch(reject)
      }, 500)
    }

    let tokenToAuth

    //Use case: Token provided by _token= (Only process once)
    if (payload.token) {
      scope.syncInProgress = true
      try {
        tokenToAuth = payload.token
        let decoded = jwt_decode(payload.token)

        if (decoded.to_client) {
          //Use case: V2 (login as child client) --> V3 : Use will not be able to select a client again (Not implemented)
          shouldLog &&
            console.log('syncLogged::_token::login-as', decoded.to_client)
          userInfos.toClientToken = payload.token
        } else {
          shouldLog &&
            console.log('syncLogged::_token::simple', decoded.to_client)
          userInfos.token = payload.token //i,g V2 --> V3
        }
        await cache.setItem('user_infos', userInfos) //Write to cache ASAP because they might be multipe syncLogged calls and API requests that require JWT
        scope.syncInProgress = false
      } catch (err) {
        scope.syncInProgress = false
        throw err
      }
    } else {
      userInfos = (await cache.getItem('user_infos')) || {}
      tokenToAuth = userInfos.token
    }

    shouldLog && console.log('syncLogged::tokenToAuth', tokenToAuth)

    if (tokenToAuth) {
      try {
        let decoded = jwt_decode(tokenToAuth)

        //Keep using an existing token without server check (expiration check is done in the client) (++performance) (api check takes ~5s)
        if (
          moment().isBefore(moment(new Date(decoded.exp * 1000)), 'minute') &&
          Object.keys(userInfos).length > 1
        ) {
          shouldLog && console.log('syncLogged::token-still-valid')

          return syncLoggedUsingExistingInfos({ userInfos })
        }

        let check_token = await api.post('/public/check_token', {
          token: tokenToAuth,
        })
        /*shouldLog &&
          console.log('syncLogged.', {
            check_token: check_token.data,
          })*/
        if (check_token.data.userId) {
          shouldLog && console.log('auth::success::check-token')
          let infos = await buildUserInfos(
            { data: userInfos }, // token will be passed by here: i.g {token:xxx} or {toClientToken:xxx}
            //{ data: tokenToAuth },
            null,
            check_token
          )

          if (infos.rights.length === 0) {

            alertStore.addAlert(
              {
                title: '403',
                text: 'alerts.NOT_ENOUGH_RIGHTS',
                type: 'warning',
              }
            )
            throw new Error('AUTH_NO_RIGHTS')
          }

          await authenticateUser(infos)

          shouldLog && console.log('auth login-ok (token regen)')

          return true
        } else {
          shouldLog && console.log('auth::fail::check-token')
        }
      } catch (err) {
        console.error(err)
      }
    }

  }

  async function logout() {
    await apiStorage.clear()
    window._logoutAt = Date.now()
  }

  async function authenticateUser(userInfos) {
    shouldLog && console.log('auth::authenticateUser')
    await cache.setItem('user_infos', userInfos)
    let currentLanguage = settingsStore.getParameter('applicationLanguage')
    if (currentLanguage) {
      //Persist the selected language in the login view (Priorized over client country language)
      //This should be done before setLogged mutation, otherwise components will try to syncCache right away.
      await saveSettingsParamLocally('applicationLanguage', currentLanguage, {
        suffix: encodeLoginNameClientId(
          userInfos.user || userInfos.login,
          userInfos.clientId || userInfos.client_id
        ),
      })
    }
    ; (async () => {
      //Update settings from cache, then from server
      await settingsStore.syncFromCache()
      await settingsStore.syncFromServer()
    })()

    setLogged(userInfos)
    auditSrv.createSession({
      jwt: userInfos.toClientToken || userInfos.token,
    })

    return true
  }

  /**
     * Set a new session on the fly. It will retrieve rights from API.
     * @function syncLoggedUsingExistingInfos
     * @param {Object} payload.userInfos new session information
     * @returns
     */
  async function syncLoggedUsingExistingInfos(

    { userInfos, ...options }
  ) {
    await cache.setItem('user_infos', userInfos) //To be able to fetch rigths
    let rightsResponse = await getUserRightsNew(
      userInfos.token || userInfos.toClientToken
    )
    userInfos.externalRights = rightsResponse.externalRights
    userInfos.rights = rightsResponse.rights
    authenticateUser(userInfos)
    return true
  }




  return {
    state,
    loginAs,
    clientCountry,
    loginName,
    loginNameClientIdEncoded,
    isLogged,
    isLoginAs,
    rightsList,
    setLogged,
    login,
    syncLogged,
    logout
  }

})
export default useAuthStore;



// Reusable logic extracted to functions

async function getUserRightsNew(jwt) {
  shouldLog && console.log('auth::getUserRightsNew::jwt', jwt)
  let rightsResponse = (
    await api.v3.get(
      `${APIUrls.APIV3_USER_ACCESS_RIGHTS}?typeSectionId=12`,
      {},
      {
        jwt,
      }
    )
  ).data
  let rights = rightsResponse.rights.map((item) => item.code)
  if (rightsResponse.commonRights) {
    rights = rights.concat(rightsResponse.commonRights.map((r) => r.code))
  }
  return {
    rights,
    externalRights: rightsResponse.externalRights,
  }
}

async function getUserRights(jwt) {
  shouldLog && console.log('auth::getUserRightsNew::jwt', jwt)
  let rightsResponse = (
    await api.v3.get(
      `${APIUrls.APIV3_USER_ACCESS_RIGHTS}?typeSectionId=12`,
      {},
      {
        jwt,
      }
    )
  ).data
  let rights = rightsResponse.rights.map((item) => item.code)
  if (rightsResponse.commonRights) {
    rights = rights.concat(rightsResponse.commonRights.map((r) => r.code))
  }
  return {
    rights,
    externalRights: rightsResponse.externalRights,
  }
}


/**
 * Those informations are stored locally (client-cache)
 * 21-01-22: API check_token "token" should override existing token (To handle rights when redirecting applications, for example: V3 -> Citifret)
 * @todo Refactor or replace by syncLoggedUsingExistingInfos
 */
async function buildUserInfos(
  userInfos = {},
  tokenInfos = {},
  checkTokenInfos = {}
) {
  let token =
    (checkTokenInfos.data || {}).token ||
    tokenInfos?.data ||
    (userInfos?.data || {}).token

  shouldLog &&
    console.log('auth::buildUserInfos::token', {
      token,
      userInfos,
      tokenInfos,
      checkTokenInfos,
    })

  let anyJWT = token || (userInfos?.data || {}).toClientToken

  //New grab rights from new api
  let rightsResponse = await getUserRightsNew(
    token || (userInfos?.data || {}).toClientToken
  )

  shouldLog &&
    console.log('buildUserInfos', {
      userInfos,
      tokenInfos,
      checkTokenInfos,
    })

  return {
    rights: rightsResponse.rights,
    externalRights: rightsResponse.externalRights,
    ...userInfos.data,
    ...(checkTokenInfos.data || {}),
    token, //parent token only (token should not contain to_client attribute)
    toClientToken: userInfos?.data?.toClientToken || '', //child token (login-as feature)
    toClientId: userInfos?.data?.toClientId || '',
    hasChildClients: userInfos?.data?.childs,
    password: '#####',
    updated_at: Date.now(),
    country:
      userInfos?.data?.country ||
      (await getClientParameter('Pays', {
        jwt: anyJWT,
      })),
  }
}
