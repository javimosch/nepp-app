export function postFormData(url, payload, options = {}) {
    const formData = new URLSearchParams();
    for (const key in payload) {
        formData.append(key, payload[key]);
    }

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            ...options.headers
        },
        body: formData
    };

    return fetch(url, requestOptions)
        .then(response => response.json())
}

export function post(url, payload, options = {}) {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        ...options.headers
      },
      body: JSON.stringify(payload)
    };
  
    return fetch(url, requestOptions)
      .then(response => {
        return response.json();
      });
  }
  
  