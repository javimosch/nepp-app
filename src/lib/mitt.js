import { default as createMitt } from 'mitt'


/**
 * Mitt default instance
 *
 * IMPORTANT NOTICE: Avoid using mitt as much as possible. Ask for alternative communication strategies.
 */
export const mitt = createMitt()

export default mitt
