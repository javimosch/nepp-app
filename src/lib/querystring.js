/**
 * @namespace Utils
 * @category Utils
 * @module Querystring*/

import { isUnitTest } from '@/lib/unit.js'

/**
 *
 * Parses the Vue route hash for query params.
 * Some times, route.query doesn't bring the querystring parameters as expected.
 *
 * @param {*} route
 */
export function getRouteQuery(route) {
  if (route) {
    let query = route.query || {}
    let split = route.hash.split('?')
    if (split.length > 1) {
      split = split[1].split('&')
      split.forEach((param) => {
        param = param.split('=')
        if (param.length === 2) {
          query[param[0]] = param[1]
          console.log(`getRouteQuery | set ${param[0]} to ${param[1]}`)
        }
      })
    }
    return query
  }
  return {}
}

export function getQueryStringValue(key, url) {
  if (window._fakeQSParams && window._fakeQSParams[key]) {
    return window._fakeQSParams[key]
  }
  if (isUnitTest && import.meta.env.VUE_APP_QUERYSTRING_PARAMS) {
    let matchedParam = import.meta.env.VUE_APP_QUERYSTRING_PARAMS.split('&').find(
      (p) => p.split('=')[0] == key
    )
    if (matchedParam) {
      return matchedParam.split('=')[1]
    }
  }
  return decodeURIComponent(
    (url || window.location.hash).replace(
      new RegExp(
        '^(?:.*[&\\?]' +
          encodeURIComponent(key).replace(/[\.\+\*]/g, '\\$&') +
          '(?:\\=([^&]*))?)?.*$',
        'i'
      ),
      '$1'
    )
  )
}

export function getQueryStringValueOnce(key) {
  let value = getQueryStringValue(key)
  removeParamFromQueryString(key)
  return value
}

/**
 * @deprecated
 * @todo Refactor/Remove
 * @param {*} parameter
 * @returns
 */
function removeParamFromQueryString(parameter) {
  var url = document.location.href
  var urlparts = url.split('?')

  if (urlparts.length >= 2) {
    var urlBase = urlparts.shift()
    var queryString = urlparts.join('?')

    var prefix = encodeURIComponent(parameter) + '='
    var pars = queryString.split(/[&;]/g)
    for (var i = pars.length; i-- > 0; )
      if (pars[i].lastIndexOf(prefix, 0) !== -1) pars.splice(i, 1)
    url = urlBase + '?' + pars.join('&')
    window.history.pushState('', document.title, url) // added this line to push the new url directly to url bar .
  }
  return url
}

window.removeParamFromQueryString = removeParamFromQueryString
window.getQueryStringValue = getQueryStringValue
