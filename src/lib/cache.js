import localforage from 'localforage'

localforage.config({
  //driver      : localforage.WEBSQL, // Force WebSQL; same as using setDriver()
  name: 'admin_v3',
  version: 1.0,
  //size        : 4980736, // Size of database, in bytes. WebSQL-only for now.
  //storeName   : 'keyvaluepairs', // Should be alphanumeric, with underscores.
  //description : 'some description'
})

export default {
  ...localforage,
  async setItem(key, value) {
    key = key.split('__').join('_')
    return localforage.setItem(key, value)
  },
  async getItem(key) {
    key = key.split('__').join('_')
    return localforage.getItem(key)
  },
  async removeItem(key) {
    key = key.split('__').join('_')
    return localforage.removeItem(key)
  },
  async exists(key) {
    key = key.split('__').join('_')
    let keys = await localforage.keys()
    return keys.includes(key)
  },
}
