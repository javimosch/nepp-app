import shortid from 'shortid'
/**
 * Wrap unique id generator
 * @todo Replace shortid with a new algorithm (shortid is now unsafe: https://www.npmjs.com/package/shortid )
 * @param {*} prefix
 * @returns
 */
export function generateShortId(prefix = 'id_') {
    return (prefix || '') + shortid.generate()
  }
  