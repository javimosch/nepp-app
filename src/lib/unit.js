
const isNode = () => {
  return typeof process === 'object' && process.versions && process.versions.node
}

/**
 * Sometimes we need to skip some logic if unit-test
 * Only works for JEST
 * @returns
 */
export function isUnitTest() {
    return isNode() && process?.env?.JEST_WORKER_ID !== undefined
  }
  